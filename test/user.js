//During the test the env variable is set to test
process.env.NODE_ENV = 'test';

let mongoose = require("mongoose");
let User = require('../models/user');

//Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../api');
let should = chai.should();


chai.use(chaiHttp);

//Our parent block
describe('Users', () => {
    beforeEach((done) => { //Before each test we empty the database
        User.remove({}, (err) => {
            done();
        });
    });
    /*
     * Test the /GET route
     */
    describe('/GET user', () => {
        it('it should GET all the books', (done) => {
            chai.request(server)
                .get('/user')
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    res.body.length.should.be.eql(0);
                    done();
                });
        });
    });
    /*
     * Test the /POST route
     */
    describe('/POST user', () => {
        it('it should not POST a user without password field', (done) => {
            let book = {
                name: "Deepak Sharma",
                email: "deepak.s@gmail.com",
                mobile: 7788990077
            }
            chai.request(server)
                .post('/user')
                .send(book)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('errors');
                    res.body.errors.should.have.property('password');
                    res.body.errors.password.should.have.property('kind').eql('required');
                    done();
                });
        });
        it('it should POST a book ', (done) => {
            let book = {
                name: "Dee",
                email: "asd@gmail.com",
                mobile: 6677889900,
                password: 1170
            }
            chai.request(server)
                .post('/user')
                .send(book)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('message').eql('User successfully added!');
                    res.body.user.should.have.property('name');
                    res.body.user.should.have.property('email');
                    res.body.user.should.have.property('mobile');
                    res.body.user.should.have.property('password');
                    done();
                });
        });
    });
    /*
     * Test the /GET/:id route
     */
    describe('/GET/:id user', () => {
        it('it should GET a user by the given id', (done) => {
            let user = new User({ name: "Deessss",
                email: "asdss@gmail.com",
                mobile: 66666889900,
                password: 66666 });
            user.save((err, user) => {
                chai.request(server)
                    .get('/user/' + user.id)
                    .send(user)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.a('object');
                        res.body.should.have.property('name');
                        res.body.should.have.property('email');
                        res.body.should.have.property('mobile');
                        res.body.should.have.property('password');
                        res.body.should.have.property('_id').eql(user.id);
                        done();
                    });
            });

        });
    });
    /*
     * Test the /PUT/:id route
     */
    describe('/PUT/:id user', () => {
        it('it should UPDATE a user given the id', (done) => {
            let book = new User({title: "The Chronicles of Narnia", author: "C.S. Lewis", year: 1948, pages: 778})
            book.save((err, book) => {
                chai.request(server)
                    .put('/book/' + book.id)
                    .send({title: "The Chronicles of Narnia", author: "C.S. Lewis", year: 1950, pages: 778})
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.a('object');
                        res.body.should.have.property('message').eql('Book updated!');
                        res.body.book.should.have.property('year').eql(1950);
                        done();
                    });
            });
        });
    });
    /*
     * Test the /DELETE/:id route
     */
    describe('/DELETE/:id book', () => {
        it('it should DELETE a book given the id', (done) => {
            let book = new Book({title: "The Chronicles of Narnia", author: "C.S. Lewis", year: 1948, pages: 778})
            book.save((err, book) => {
                chai.request(server)
                    .delete('/book/' + book.id)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.a('object');
                        res.body.should.have.property('message').eql('Book successfully deleted!');
                        res.body.result.should.have.property('ok').eql(1);
                        res.body.result.should.have.property('n').eql(1);
                        done();
                    });
            });
        });
    });
});
  